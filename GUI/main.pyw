from turtle import down
import PySimpleGUI as sg
from yt_dlp import YoutubeDL
import threading


class sub(threading.Thread):
    url = ""

    def run(self):
        window["now"].update("ダウンロード開始")
        Sub = download()
        Sub.start()
        Sub.join()
        if download.flg == False:
            window["now"].update("ダウンロード終了")


class download(threading.Thread):
    flg = False
    url = ""

    def run(self):
        download.flg = False
        download.DL()

    def DL():
        try:
            youtube = YoutubeDL({"format": "mp4"})
            youtube.download(download.url)
        except:
            download.flg = True
            window["now"].update("エラー")


# 画面構成
layout = [
    [sg.Text("YouTube Download Test")],
    [sg.HorizontalSeparator()],
    [sg.Text()],
    [sg.Text("進行状況："), sg.Text(key="now")],
    [sg.Input(key="url"), sg.Button("Download", key="dl")]
]

# ウィンドウの生成
window = sg.Window("フォームタイトル", layout)

# イベントループ
while True:
    event, values = window.read()

    if event == sg.WIN_CLOSED:
        # ウィンドウが閉じられた時に処理を抜ける
        break
    elif event == "dl":
        download.url = values["url"]
        Sub = sub()
        Sub.start()


window.close()
