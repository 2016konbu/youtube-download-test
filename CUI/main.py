from yt_dlp import YoutubeDL
import time

url = "https://www.youtube.com/watch?v=sToRddIV7kU"

try:
    youtube = YoutubeDL({"format": "mp4"})
    youtube.download(url)
    youtube.download_with_info_file(url)
except:
    print("error!")
