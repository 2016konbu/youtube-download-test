# はじめに
&emsp;このファイルはyt-dlpのYouTubeDL.pyにあった説明文を日本語に翻訳したものです。<br>
&emsp;手作業で翻訳したのでミスがあるかもしれません。

# 説明

&emsp;YoutubeDL オブジェクトは、実際のビデオファイルをダウンロードし、ユーザーが要求した場合にはそれをディスクに書き込むなどの作業を担当するものです。ほとんどの場合、1 つのプログラムに 1 つ存在する必要があります。ビデオの URL が与えられると、ダウンローダは必要な情報をすべて抽出する方法を知らないので、InfoExtractors が行うタスクの 1 つに URL を渡さなければなりません。<br><br>
&emsp;このため、YoutubeDLオブジェクトには、InfoExtractorを所定の順序で登録できるメソッドが用意されています。URL が渡されると、YoutubeDL オブジェクトは、それを処理できると報告した最初の InfoExtractor にそれを処理します。InfoExtractor は URL が参照するビデオに関するすべての情報を抽出し、YoutubeDL は抽出された情報を処理し、場合によっては File Downloader を使用してビデオをダウンロードします。<br><br>
&emsp;YoutubeDL オブジェクトは多くのパラメータを受け取ります。オブジェクトのコンストラクタが引数でいっぱいにならないように、代わりにオプションの辞書を受け取ります。これらのオプションは、InfoExtractorsが使用するためにparams属性を通じて利用可能です。YoutubeDLはまた、それに追加されるInfoExtractorの担当ダウンローダーとして自分自身を登録するので、これは「相互登録」となります。<br><br><br><br>

# 利用可能なオプション

## username
&emsp;認証のためのユーザー名。<br>

## password
&emsp;認証のためのパスワード。<br>

## videopassword
&emsp;ビデオにアクセスするためのパスワードです。<br>

## ap_mso
&emsp;Adobe Pass 複数システム運用者識別子。<br>

## ap_username
&emsp;複数システムオペレーターアカウントのユーザー名。<br>

## ap_password
&emsp;複数システムオペレーターアカウントのパスワード。<br>

## usenetrc
&emsp;代わりにnetrcを認証に使用します。<br>

## verbose
&emsp;標準出力に追加情報を表示する。<br>

## quiet
&emsp;stdoutにメッセージを出力しない。<br>

## no_warnings
&emsp;警告の場合は何もプリントアウトしない。<br>
&emsp;forceprint:stdoutに出力するテンプレートのリストにマッピングされたWHENのキーを持つdictです。許可されるキーは、video または utils.POSTPROCESS_WHEN にある項目のいずれかです。<br>
&emsp;互換性のために、単一のリストも受け入れられます。<br>

## print_to_file
&emsp;WHEN（forceprintと同じ）をキーとするdictが(template, filename)を持つタプルのリストにマッピングされたもの。<br>

## forcejson
&emsp;info_dictをJSONとして強制的に出力する。<br>

## dump_single_json
&emsp;プレイリスト（または動画）全体の info_dict を 1 行の JSON として強制的に出力します。<br>

## force_write_download_archive
&emsp;'skip_download' や 'simulate' に関係なく、ダウンロードアーカイブを強制的に書き込むようにしました。<br>

## simulate
&emsp;動画ファイルをダウンロードしない。未設定（または None）の場合、listsubtitles、listformats、list_thumbnails のいずれかが使用された場合のみシミュレートします format:Video フォーマットコード。<br>
&emsp;また、関数を渡すこともできます。この関数は'ctx'を引数として取り、ダウンロードするフォーマットを返します。<br>
&emsp;実装は "build_format_selector "を参照してください。<br>

## allow_unplayable_formats
&emsp;再生不可能なフォーマットを抽出してダウンロードできるようにする。<br>

## ignore_no_formats_error
&emsp;ビデオフォーマットがありません "というエラーを無視します。実際にダウンロードできない動画でもメタデータを抽出するのに便利です（実験的）。<br>

## format_sort
&emsp;ビデオフォーマットをソートするためのフィールドのリストです。<br>
&emsp;詳しくは「フォーマットの並べ替え」をご覧ください。<br>

## format_sort_force
&emsp;与えられたformat_sortを強制的に実行する。詳細は「フォーマットのソート」を参照。<br>

## prefer_free_formats
&emsp;同じ品質の動画フォーマットで、無料コンテナ付きのものを優先するかどうか。<br>

## allow_multiple_video_streams
&emsp;複数のビデオストリームを1つのファイルに結合できるようにする。<br>

## allow_multiple_audio_streams
&emsp;複数のオーディオストリームを1つのファイルにマージすることを許可する check_formats フォーマットがダウンロード可能であるかどうかをテストするかどうか。<br>
&emsp;True (全てチェック)、False (何もチェックしない)、 'selected' (選択したフォーマットをチェック)、None (extractor が要求した場合のみチェック) のいずれかを指定することができる。<br>

## paths
&emsp;出力パスの辞書。許可されるキーは 'home' 'temp' と OUTTMPL_TYPES (in utils.py) のキーです。<br>

## outtmpl
&emsp;出力名のテンプレートの辞書です。使用可能なキーは 'default' と OUTTMPL_TYPES (in utils.py) のキーです。<br>
&emsp;youtube-dlとの互換性のために、単一の文字列を使用することもできます。<br>

## outtmpl_na_placeholder
&emsp;利用できないメタフィールドのためのプレースホルダー。<br>

## restrictfilenames
&emsp;ファイル名に"&"やスペースを使用しない trim_file_name:ファイル名の長さを制限する(拡張子は除く)

## windowsfilenames
&emsp;ファイル名を強制的にWindows互換にする

## ignoreerrors
&emsp;ダウンロードや後処理のエラーで停止しない。<br>
&emsp;only_download'を指定すると、ダウンロードエラーのみを無視することができる。<br>
&emsp;デフォルトはCLIでは'only_download'、APIではFalse。<br>

## skip_playlist_after_errors
&emsp;プレイリストの残りをスキップするまでの許容失敗回数

## allowed_extractors
extractor：名に対して許可される正規表現リスト overwrites：真の場合はすべての動画とメタデータファイルを上書きし、None の場合は動画以外のファイルのみを上書きし、False の場合はどのファイルも上書きしない youtube-dl との互換性のために、代わりに "nooverwrites" も使用可能。<br>
playlist_items：download するプレイリストの特定のインデックスを指定します。<br>
playlistrandom：ランダムな順序でプレイリストアイテムをダウンロードします。<br>

## lazy_playlist
&emsp;プレイリストのエントリーを受信して処理する。<br>
matchtitle：一致するタイトルのみをダウンロードします。<br>

## rejecttitle
&emsp;タイトルが一致するダウンロードを拒否する。<br>
logger:メッセージをlogging.Loggerインスタンスに記録します。<br>

## logtostderr
&emsp;標準出力ではなく標準エラー出力にすべてを出力する。<br>

## consoletitle
&emsp;コンソールウィンドウのタイトルバーに進捗状況を表示する。<br>

## writedescription
&emsp;動画の説明を.descriptionファイルに書き込む

## writeinfojson
&emsp;動画の説明を.info.json ファイルに書き出す。<br>
clean_infojson:infojsonからプライベートフィールドを削除する

## getcomments
&emsp;動画コメントを抽出する。writeinfojson が指定されていない限り、ディスクには書き込まれません。<br>

## writeannotations
&emsp;ビデオのアノテーションを.annotations.xml ファイルに書き込む
writethumbnail:サムネイル画像をファイルに書き出す

## allow_playlist_files
&emsp;write*' オプション使用時にプレイリストの説明やinfojsonなどもディスクに書き込むかどうか。<br>

## write_all_thumbnails
&emsp;すべてのサムネイル形式をファイルに書き込む

## writelink
&emsp;現在のプラットフォームに応じて、インターネットショートカットファイルを書き込む（.url/.webloc/.desktop）

## writeurllink
&emsp;Windowsのインターネットショートカットファイル(.url)を書き込む

## writewebloclink
&emsp;macOSのインターネットショートカットファイル(.webloc)を書き込む

## writedesktoplink
&emsp;Linuxのインターネットショートカットファイル(.desktop)を書き込む。<br>
writesubtitles:ビデオ字幕をファイルに書き込む

## writeautomaticsub
&emsp;自動生成された字幕をファイルに書き出す

## listsubtitles
&emsp;ビデオで利用可能なすべての字幕をリストアップします

## subtitlesformat
&emsp;字幕のフォーマットコード<br>
subtitleslangs:ダウンロードする字幕の言語のリスト（正規表現も可）。リストには、利用可能なすべての字幕を参照するために "all" を含めることができます。言語は、要求された言語から除外するために、"-" を前に付けることができます（例： ['all', '-live_chat'] ）。<br>

## keepvideo
&emsp;ポストプロセッシング後のビデオファイルを保持

## daterange
&emsp;DateRange オブジェクト。upload_date が範囲内にある場合のみダウンロードする。<br>

## skip_download
&emsp;ビデオファイルの実際のダウンロードをスキップする

## cachedir
&emsp;ファイルシステム内のキャッシュファイルの場所。<br>
&emsp;ファイルシステムのキャッシュを無効にする場合は False を指定します。<br>
noplaylist:疑わしい場合は、プレイリストの代わりに単一のビデオをダウンロードします。<br>

## age_limit
&emsp;ユーザーの年齢を表す整数値。<br>
&emsp;指定した年齢に合わない動画はスキップされます。<br>

## min_views
&emsp;動画がスキップされないために必要な最小の視聴回数を表す整数値。<br>
&emsp;視聴回数情報のない動画は、常にダウンロードされます。制限しない場合は None。<br>

## max_views
&emsp;最大視聴回数を表す整数値。<br>
&emsp;それよりも人気のある動画はダウンロードされません。<br>
&emsp;視聴回数情報のない動画は、常にダウンロードされます。制限なしの場合はNone。<br>

## download_archive
&emsp;セット、またはすべてのダウンロードが記録されるファイルの名前。<br>
&emsp;ファイル内にすでに存在するビデオは、再度ダウンロードされません。<br>

## break_on_existing
&emsp;アーカイブ内にあるファイルをダウンロードしようとした後、ダウンロード処理を停止する。<br>

## break_on_reject
&emsp;フィルタリングされた動画に遭遇した場合、ダウンロード処理を停止する。<br>

## break_per_url
&emsp;break_on_reject と break_on_existing が、キュー全体ではなく、それぞれの入力 URL に対して作用するかどうかを指定します。<br>
cookiefile:クッキーを読み込んでダンプする場所のファイル名またはテキストストリーム。<br>

## cookiesfrombrowser
&emsp;ブラウザ名、クッキーを読み込むプロファイル名/パス、キーリング名、コンテナ名を含むタプル、例えば ('chrome', ) または ('vivaldi', 'default', 'BASICTEXT') または ('firefox', 'default', None, 'Meta')

## legacyserverconnect
&emsp;RFC 5746のセキュアリネゴシエーションをサポートしないサーバーへのHTTPS接続を明示的に許可する。<br>

## nocheckcertificate
&emsp;SSL証明書を検証しない

## client_certificate
&emsp;PEM 形式のクライアント証明書ファイルへのパス。秘密鍵を含む場合があります。<br>

## client_certificate_key
&emsp;クライアント証明書の秘密鍵ファイルへのパス

## client_certificate_password
&emsp;クライアント証明書の秘密鍵のパスワード (暗号化されている場合)。<br>
&emsp;もし提供されず、鍵が暗号化されている場合、yt-dlpは対話的に尋ねます。<br>

## prefer_insecure
&emsp;HTTPSの代わりにHTTPを使用して情報を取得する。<br>
&emsp;(一部の抽出器のみ対応）

## http_headers
&emsp;すべてのリクエストで使用されるカスタムヘッダの辞書

## proxy
&emsp;使用するプロキシサーバーのURL

## geo_verification_proxy
&emsp;地域制限のあるサイトでIPアドレスの確認に使用するプロキシのURL。<br>
socket_timeout:応答がないホストを待つ時間（秒単位

## bidi_workaround
&emsp;双方向テキストをサポートしないバギーな端末を、fridibi を使って回避する。<br>
debug_printtraffic:送受信されたHTTPトラフィックを出力します。<br>
default_search:入力されたURLが有効でない場合、この文字列を前置します。推測の精度を上げるには'auto'を指定します。<br>

## encoding
&emsp;システムで指定されたエンコーディングの代わりに、このエンコーディングを使用します。<br>

## extract_flat
&emsp;url_results を解決し、さらに処理するかどうか。<br>
* False: 常に処理する（デフォルト）
* True:  ネバープロセス
* 'in_playlist': プレイリスト/multi_video内では処理しない。<br>
* 'discard': 常に処理するが、プレイリスト/multi_video 内から結果を返さない。<br>
* 'discard_in_playlist': discard "と同じだが、プレイリストにのみ適用される（multi_videoには適用されない）。<br>
wait_for_video:与えられた場合、スケジュールされたストリームが利用可能になるのを待ちます。この値は、再試行までの待ち時間の範囲 (min_secs, max_secs) を含むタプルである必要があります。<br>
postprocessors:辞書のリストで、それぞれがエントリ
* key:  ポストプロセッサの名前です。一覧は yt_dlp/postprocessor/__init__.py を参照してください。<br>
* when: いつポストプロセッサを実行するか。許容される値は utils.POSTPROCESS_WHEN のエントリです。 与えられなければ 'post_process' と仮定されます。<br>
progress_hooks:ダウンロードの進行状況に応じて呼び出される関数のリスト。<br>
* status: "downloading", "error", "finished "のいずれか。最初にこれを確認し、不明な値は無視する。<br>
* info_dict: 抽出された info_dict ステータスが "downloading", "finished" のいずれかであれば
* following properties may also be present:
* filename: 最終的なファイル名(常に存在する)
* tmpfilename: 現在書き込んでいるファイル名
* downloaded_bytes: ディスク上のバイト数
* total_bytes: ファイル全体のサイズ、不明な場合はなし
* total_bytes_estimate: 最終的なファイルサイズを推測、利用できない場合は None。<br>
* elapsed: ダウンロード開始からの秒数。<br>
* eta: 推定時間（秒）、不明の場合はなし
* speed: ダウンロード速度（バイト/秒）、不明の場合はなし
* fragment_index: 現在ダウンロードされているビデオフラグメントのカウンタ。<br>
* fragment_count: フラグメントの数 (= マージされる個々のファイル)。<br>
&emsp;プログレスフックは、ダウンロードが成功した場合、少なくとも一度はステータスが "finished" で呼び出されることが保証されています。<br>

## postprocessor_hooks
&emsp;ポスト処理の進捗に応じて呼び出される関数のリストで、エントリーは以下のような辞書です。<br>
* status: "started"、"processing"、"finished "のいずれかを指定する。<br>
まずこれを確認し、不明な値は無視する。<br>
* postprocessor: ポストプロセッサの名称
* info_dict: 抽出されたinfo_dict Progressフックは、処理が成功した場合、少なくとも2回（ステータスが "started "と "finished"）呼ばれることが保証されます。<br>

## merge_output_format
&emsp;フォーマットをマージする際に使用する拡張子のリストを "/" で区切ったもの。<br>

## final_ext
&emsp;期待される最終拡張子。ファイルがすでにダウンロードされ、変換されたことを検出するために使用されます。<br>

## fixup
&emsp;ファイルの既知の不具合を自動的に修正します。<br>

## One of
   - "never": 何もしない
   - "warn": 警告を発するのみ
   - "detect_or_warn": 何かできるかどうかを確認し、そうでなければ警告する (デフォルト)

source_address:バインド先となるクライアント側のIPアドレス。<br>

## sleep_interval_requests
&emsp;抽出中の要求の間にスリープさせる秒数<br>
sleep_interval:単独で使用する場合は、各ダウンロードの前にスリープする秒数、または max_sleep_interval と共に使用する場合は、各ダウンロードの前にランダムにスリープする範囲の下限 (スリープする秒数の最小値) を指定する。<br>
max_sleep_interval:各ダウンロード前のランダムスリープの範囲の上限値（スリープ可能な最大秒数）。sleep_interval と共に使用する必要があります。実際の睡眠時間は、範囲 [sleep_interval; max_sleep_interval] からのランダムな浮動小数点数である。<br>

## sleep_interval_subtitles
&emsp;各サブタイトルダウンロードの前にスリープする秒数

## listformats
&emsp;利用可能なビデオフォーマットの概要を表示し、終了します。<br>

## list_thumbnails
&emsp;すべてのサムネイルのテーブルを表示して終了します。<br>

## match_filter
&emsp;(info_dict, *, incomplete: bool) -> Optional[str] というシグネチャを持つビデオごとに呼び出される関数。<br>
youtube-dlとの後方互換性のため、(info_dict) -> Optional[str]のシグネチャも許容されます。<br>
- メッセージを返した場合、ビデオは無視される。<br>
- None を返す場合、ビデオはダウンロードされる。<br>
- utils.NO_DEFAULTを返した場合、ユーザは動画をダウンロードするかどうかを対話的に尋ねられます。<br>
utils.pyのmatch_filter_funcは、このための一つの例です。<br>

## no_color
&emsp;出力にカラーコードを出力しない。<br>
geo_bypass:X-Forwarded-ForのHTTPヘッダを偽装して地理的な制限を回避する。<br>

## geo_bypass_country
&emsp;X-Forwarded-For HTTP ヘッダーの偽造による明示的な地理的制限の回避に使用される ISO 3166-2 の 2 文字の国コード。<br>

## geo_bypass_ip_block
&emsp;geo_bypass_country と同様に使用される、CIDR 表記による IP 範囲。<br>

## external_downloader
&emsp;プロトコルのキーと、それに使用する外部ダウンローダーの実行ファイルの辞書。許可されるプロトコルは default|http|ftp|m3u8|dash|rtsp|rtmp|mms です。ネイティブダウンローダを使用する場合は、この値を'native'に設定します。<br>

## compat_opts
&emsp;互換性のあるオプションです。デフォルトの動作の違い」を参照してください。<br><br>
&emsp;以下のオプションは、APIを通して使用する場合、機能しません。<br>
+ filename
+ abort-on-error
+ multistreams
+ no-live-chat
+ format-sort
no-clean-infojson
+ no-playlist-metafiles
+ no-keep-subs
+ no-attach-info-json.

実装は \_\_init\_\_.py を参照してください。<br>

## progress_template
&emsp;進捗状況出力用のテンプレートの辞書。<br>
&emsp;許可されるキーは 'download', 'postprocess', 'download-title' (コンソールタイトル), 'postprocess-title' です。<br>
&emsp;テンプレートは、キー 'progress' と 'info' を持つ辞書にマッピングされます。<br>

## retry_sleep_functions
&emsp;試行回数を引数にとり、スリープまでの時間を秒単位で返す関数のディクショナリ。<br>
&emsp;許可されるキーは 'http', 'fragment', 'file_access' である表示。<br>

## download_ranges
&emsp;(info_dict, ydl) -> Iterable[Section] のシグネチャを持つ、すべての動画に対して呼び出されるコールバック関数です。返却された部分のみダウンロードされます。<br><br>
各セクションは、以下のキーを持つディクショナリーです表示。<br>
* start_time: 区間開始時刻 (秒)
* end_time: 区間終了時刻 (秒)
* title: セクションタイトル（オプション）
* index: セクション番号（オプション）

## force_keyframes_at_cuts
&emsp;範囲をダウンロードする際にビデオを再エンコードすることで、正確なカットを得ることができる表示。<br>
noprogress:プログレスバーを表示しない

## live_from_start
ライブ配信の動画を最初からダウンロードするかどうか表示。<br>
以下のパラメータは YoutubeDL 自身が使用するものではなく、ダウンローダーが使用するものです表示。<br>
ダウンローダー (yt_dlp/downloader/common.py を参照) で使用されます表示。<br>
+ nopart
+ updatetime
+ buffersize
+ ratelimit
+ throttledratelimit
+ min_filesize
+ max_filesize
+ test
+ noresizebuffer
+ retries
+ file_access_retries
+ fragment_retries
+ continuedl
+ xattr_set_filesize
+ hls_use_mpegts
+ http_chunk_size
+ external_downloader_args
+ concurrent_fragment_downloads

<br><br>

# ポストプロセッサでは、以下のオプションが使用されます。<br>

## ffmpeg_location
&emsp;ffmpeg/avconv バイナリの場所。 バイナリへのパスまたはそれを含むディレクトリ表示。<br>
## postprocessor_args
&emsp;ポストプロセッサ/実行可能ファイルのキー(小文字)と、ポストプロセッサ/実行可能ファイルに対する追加のコマンドライン引数のリストの辞書です。dict は、与えられた exe が与えられた PP によって使用されるときに使用される "PP+EXE" キーを持つことも可能です表示。<br>
&emsp;すべての PP に渡される引数の名前として 'default' を使用する youtube-dl との互換性のために、単一の引数のリストを使用することもできます表示。<br>

<br><br>

# 抽出器では以下のオプションが使用されます表示。<br>

## extractor_retries
&emsp;既知のエラーに対する再試行回数

## dynamic_mpd
&emsp;動的な DASH マニフェストを処理するかどうか (デフォルト: True)

## hls_split_discontinuity
&emsp;広告の切れ目などの不連続部分でHLSプレイリストを異なるフォーマットに分割する（デフォルト：False）

## extractor_args
&emsp;抽出器に渡される引数の辞書表示。<br>
&emsp;詳細は "EXTRACTOR ARGUMENTS" を参照すること表示。<br>
&emsp;E.g. {'youtube': {'skip': ['dash', 'hls']}}

## mark_watched
&emsp;視聴したビデオをマークする（-simulateでも可）。YouTubeのみ

<br><br>

# 以下のオプションは非推奨であり、将来的に削除される可能性があります表示。<br>

## force_generic_extractor
&emsp;ダウンローダーが一般的な抽出器を使用するように強制する
- allowed_extractors = ['generic', 'default'] を使用する表示。<br>

## playliststart
&emsp;playlist_items 開始するプレイリスト項目表示。<br>

## playlistend
&emsp;playlist_items 終了するプレイリスト項目表示。<br>

## playlistreverse
&emsp;playlist_items を使用する プレイリストの項目を逆順にダウンロードする表示。<br>

## forceurl
&emsp;forceprint を使用する 最終的な URL を強制的に印刷する表示。<br>

## forcetitle
&emsp;forceprint を使用する 強制的にタイトルを印刷する表示。<br>

## forceid
&emsp;forceprint を使用する 強制的に ID を印刷する表示。<br>

## forcethumbnail
&emsp;forceprint を使用する サムネイル URL を強制印刷する表示。<br>

## forcedescription
&emsp;forceprint を使用する 強制印刷の記述表示。<br>

## forcefilename
&emsp;forceprintを使用する 最終的なファイル名を強制的に印刷する表示。<br>

## forceduration
&emsp;Use forceprint 強制的に印刷する期間を設定する表示。<br>

## allsubtitles
&emsp;subtitleslangs = ['all'] を使用します表示。<br>
&emsp;ビデオのすべての字幕をダウンロードします
&emsp;(writesubtitles または writeautomaticsub が必要です)

## include_ads
&emsp;動作しない 広告もダウンロードする

## call_home
&emsp;未実装<br>
&emsp;デバッグのために yt-dlp サーバーに問い合わせることが許可されている場合は true表示。<br>

## post_hooks
&emsp;カスタムのポストプロセッサを登録します表示。<br>
&emsp;すべてのポストプロセッサが呼び出された後に、各ビデオ ファイルの最終段階として呼び出される関数のリスト。ファイル名が唯一の引数として渡される表示。<br>

## hls_prefer_native
external_downloaderを使用する = {'m3u8': 'native'} or {'m3u8': 'ffmpeg'}<br>
&emsp;Trueならffmpeg/avconvの代わりにネイティブのHLSダウンローダーを使用し、 Falseならffmpeg/avconvを使用し、 Noneなら抽出器が提案するダウンローダーを使用します表示。<br>

## prefer_ffmpeg
&emsp;avconv のサポートは非推奨です表示。<br>
&emsp;False の場合、ffmpeg の代わりに avconv を使用し、そうでなければ ffmpeg を使用します表示。<br>

## youtube_include_dash_manifest
&emsp;extractor_args を使用します表示。<br>
&emsp;True（デフォルト）の場合、DASH マニフェストおよび関連データが抽出器によってダウンロードされ、処理されます表示。<br>
&emsp;DASHを気にしない場合は無効にして、ネットワークI/Oを減らすことができます。(youtubeの場合のみ)

## youtube_include_hls_manifest
&emsp;extractor_args を使用します表示。<br>
&emsp;True (デフォルト) の場合、HLS マニフェストと関連データがダウンロードされ、extractor で処理されます表示。<br>
&emsp;HLSを気にしない場合は無効化することでネットワークI/Oを減らすことができます。(youtubeの場合のみ)